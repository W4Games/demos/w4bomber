extends "../supabase/auth.gd"

const Endpoint = preload("../supabase/endpoint.gd")

var idp : Endpoint

func _init(client, identity):
	super(client, "/auth/v1", identity)
	idp = Endpoint.new(client, "/idp/v1", identity)

func login_device(id, key):
	return W4GD.client.auth.begin_sso(
		"anon.localhost"
	).then(func(result):
		if result.is_error():
			return result
		var split : PackedStringArray = result["url"].as_string().split("?")
		if split.size() < 2:
			return result
		var request_query = client.dict_from_query(split[1])
		return idp.POST("/sso", {
			"provider": "device",
			"id": id,
			"key": key,
		}, request_query)
	).then(func(result):
		if result.is_error():
			return result
		return W4GD.client.auth.login_sso(result.as_dict())
	)
