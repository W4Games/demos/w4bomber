extends Node

# Default game server port. Can be any number between 1024 and 49151.
# Not on the list of registered or common ports as of November 2020:
# https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers
const DEFAULT_PORT = 10567

# Max number of players.
const MAX_PEERS = 12

var peer = null
var in_game = false

# Name for my player.
var player_name = "The Warrior"
var player_id := ""
var player_password := ""

# Names for remote players in id:name format.
var players = {}
var players_ready = []

# Signals to let lobby GUI know what's going on.
signal connection_failed()
signal connection_succeeded()
signal game_ended()
signal game_error(what)


func _player_joined(player):
	players[player.peer_id] = player.info['player_name']


func _player_left(player):
	if has_node("/root/World"): # Game is in progress.
		if multiplayer.is_server():
			game_error.emit("Player " + players[player.peer_id] + " disconnected")
			end_game()
	else: # Game is not in progress.
		# Unregister this player.
		players.erase(player.peer_id)


func _peer_authentication_failed(p_peer_id: int):
	if p_peer_id == 1:
		# We failed to authenticate with the server.
		connection_failed.emit()


# Callback from SceneTree, only for clients (not server).
func _connected_ok():
	connection_succeeded.emit()


# Callback from SceneTree, only for clients (not server).
func _server_disconnected():
	game_error.emit("Server disconnected")
	end_game()


# Callback from SceneTree, only for clients (not server).
func _connected_fail():
	multiplayer.set_multiplayer_peer(null) # Remove peer
	connection_failed.emit()


@rpc("call_local")
func load_world():
	# Change scene.
	var world = load("res://world.tscn").instantiate()
	get_tree().get_root().add_child(world)
	var lobby = get_tree().get_root().get_node_or_null("Lobby")
	if lobby != null:
		lobby.hide()

	# Set up score.
	for pn in players:
		world.get_node("Score").add_player(pn, players[pn])
	get_tree().set_pause(false) # Unpause and unleash the game!

	in_game = true


func host_game():
	peer = ENetMultiplayerPeer.new()
	peer.create_server(DEFAULT_PORT, MAX_PEERS)
	multiplayer.set_multiplayer_peer(peer)

	# Mark this server as ready, so it can be allocated to a match.
	W4GD.game_server.set_server_state(W4GD.game_server.ServerState.READY)


func join_game(ip, port, new_player_name, new_player_id, new_player_password):
	player_name = new_player_name
	player_password = new_player_password
	player_id = new_player_id

	W4GD.game_server.start_client(player_id, player_password, {player_name = player_name})

	peer = ENetMultiplayerPeer.new()
	peer.create_client(ip, port)
	multiplayer.set_multiplayer_peer(peer)


func disconnect_from_game() -> void:
	if peer:
		peer.close()
		peer = null
	multiplayer.set_multiplayer_peer(null)


func get_player_list():
	return players.values()


func get_player_name():
	return player_name


func begin_game():
	assert(multiplayer.is_server())
	load_world.rpc()

	var world = get_tree().get_root().get_node("World")
	var player_scene = load("res://player.tscn")

	# Create a dictionary with peer id and respective spawn points, could be improved by randomizing.
	var spawn_points = {}
	var spawn_point_idx = 0
	for p in players:
		spawn_points[p] = spawn_point_idx
		spawn_point_idx += 1

	for p_id in spawn_points:
		var spawn_pos = world.get_node("SpawnPoints/" + str(spawn_points[p_id])).position
		var player = player_scene.instantiate()
		player.synced_position = spawn_pos
		player.name = str(p_id)
		player.set_player_name(player_name if p_id == multiplayer.get_unique_id() else players[p_id])
		world.get_node("Players").add_child(player)


func end_game():
	if has_node("/root/World"): # Game is in progress.
		# End it
		get_node("/root/World").queue_free()

	game_ended.emit()
	players.clear()


func _ready():
	W4GD.game_server.player_joined.connect(_player_joined)
	W4GD.game_server.player_left.connect(_player_left)
	multiplayer.peer_authentication_failed.connect(_peer_authentication_failed)
	multiplayer.connected_to_server.connect(_connected_ok)
	multiplayer.connection_failed.connect(_connected_fail)
	multiplayer.server_disconnected.connect(_server_disconnected)
	W4GD.analytics.session_running_event.connect(self._on_analytics_session_running_event)


func _on_analytics_session_running_event(p_extra_props: Dictionary):
	p_extra_props['in_game'] = in_game
