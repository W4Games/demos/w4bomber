extends Control

var lobby

func _ready():
	if W4GD.game_server.is_server():
		get_tree().change_scene_to_file("res://server.tscn")
		return

	W4GD.analytics.start_session({
		is_server = false,
	})

	gamestate.connection_failed.connect(_on_connection_failed)
	gamestate.connection_succeeded.connect(_on_connection_success)
	gamestate.game_ended.connect(_on_game_ended)
	gamestate.game_error.connect(_on_game_error)

	%LoginScreen.visible = true
	%JoinScreen.visible = false
	%PlayersScreen.visible = false
	%JoinErrorLabel.text = ""

func show_error(msg: String) -> void:
	%ErrorDialog.dialog_text = msg
	%ErrorDialog.popup_centered()

func _on_signup_button_pressed():
	var result = await W4GD.auth.signup_email(%EmailField.text, %PasswordField.text).async()
	if result.is_error():
		show_error(result.get_data().message)
		return

	_on_login_button_pressed()

func _on_login_button_pressed():
	var result = await W4GD.auth.login_email(%EmailField.text, %PasswordField.text).async()
	if result.is_error():
		show_error(result.get_data().message)
		return

	%LoginScreen.visible = false
	%JoinScreen.visible = true

	# Use the first part of the email as the default username.
	%NameField.text = %EmailField.text.split('@')[0]

func _on_host_button_pressed():
	if %NameField.text.strip_edges() == "":
		%JoinErrorLabel.text = "Invalid name!"
		return

	%JoinScreen.visible = false
	%JoinErrorLabel.text = ""

	var result = await W4GD.matchmaker.get_cluster_list().async()
	if result.is_error():
		%JoinScreen.visible = true
		show_error(str(result.as_error()))
		return

	var clusters: Array = result.as_array()
	if clusters.size() == 0:
		%JoinScreen.visible = true
		show_error('No available clusters found')
		return

	result = await W4GD.matchmaker.create_lobby(
		W4GD.matchmaker.LobbyType.DEDICATED_SERVER,
		{
			# This is used to select a specific game server region
			cluster = clusters.front(),
			props = {
				# This can be used for game specific information about the lobby
				customProperty = "a value",
				# This can be used to select a specific fleet based on its labels
				#gameServerSelectors = [{
				#	matchLabels = {
				#		"game-mode": "mode1",
				#	},
				#}],
			}
		}
	).async()
	if result.is_error():
		%JoinScreen.visible = true
		show_error(result.message.as_string())
		return

	lobby = result.get_data()

	%PlayersScreen.visible = true
	%StateLabel.text = "Awaiting players..."
	%StartButton.disabled = false

	%LobbyIdField.text = lobby['id']

	W4GD.analytics.lobby_joined(lobby['id'], { is_host = true })

	setup_lobby()
	_on_player_list_changed()

func _on_join_button_pressed():
	if %NameField.text.strip_edges() == "":
		%JoinErrorLabel.text = "Invalid name!"
		return

	var lobby_id = %JoinLobbyField.text.strip_edges()
	if lobby_id == "":
		%JoinErrorLabel.text = "Invalid lobby ID!"
		return

	%JoinScreen.visible = false
	%JoinErrorLabel.text = ""

	var result = await W4GD.matchmaker.join_lobby(lobby_id).async()
	if result.is_error():
		%JoinScreen.visible = true
		show_error(result.message.as_string())
		return

	lobby = result.get_data()

	%PlayersScreen.visible = true
	%StateLabel.text = "Awaiting players..."
	%StartButton.disabled = true

	%LobbyIdField.text = lobby_id

	W4GD.analytics.lobby_joined(lobby_id, { is_host = false })

	setup_lobby()
	_on_player_list_changed()

func setup_lobby() -> void:
	lobby.player_joined.connect(_on_player_list_changed)
	lobby.player_left.connect(_on_player_list_changed)
	lobby.received_server_ticket.connect(_on_received_server_ticket)

func _on_player_list_changed(_player_id = null):
	var players = lobby.get_players()
	players.sort()

	%PlayersList.clear()
	#%PlayersList.add_item(gamestate.get_player_name() + " (You)")
	for p in players:
		%PlayersList.add_item(p)

func _on_received_server_ticket(p_server_ticket):
	%StateLabel.text = "Joining server..."

	gamestate.join_game(p_server_ticket.ip, p_server_ticket.port, %NameField.text.strip_edges(), W4GD.get_identity().get_uid(), p_server_ticket.secret)

func _on_connection_success():
	%StateLabel.text = "Waiting for match to start..."

func return_to_join_screen():
	%LoginScreen.visible = false
	%JoinScreen.visible = true
	%PlayersScreen.visible = false
	%JoinErrorLabel.text = ""

func _on_connection_failed():
	show_error("Unable to connect to game server")

func _on_game_ended():
	gamestate.disconnect_from_game()
	show()
	return_to_join_screen()

func _on_game_error(errtxt):
	show_error(errtxt)
	return_to_join_screen()

func _on_start_pressed():
	lobby.state = W4GD.matchmaker.LobbyState.SEALED

	var result = await lobby.save().async()
	if result.is_error():
		show_error("Unable to seal the lobby")
		return

	%StateLabel.text = "Waiting for server allocation..."
