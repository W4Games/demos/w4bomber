extends Node

func _ready():
	W4GD.analytics.error.connect(self._on_analytics_error)
	W4GD.game_server.match_ready.connect(self._on_match_ready)
	gamestate.game_ended.connect(_on_game_ended)
	gamestate.game_error.connect(_on_game_error)
	gamestate.host_game()

func _on_match_ready() -> void:
	# Only start analytics once players have connected - there is no need to
	# track idle servers (especially since there's a chance they could be
	# replaced before ever having been used).
	W4GD.analytics.start_session({
		is_server = true,
		server_name = OS.get_environment('W4CLOUD_GAMESERVER_NAME'),
		player_count = W4GD.game_server.get_players().size(),
	})
	W4GD.analytics.lobby_joined(W4GD.game_server.get_server().get_lobby_id())

	print ("Lobby properties: ", W4GD.game_server.get_server().get_lobby_properties())

	gamestate.begin_game()

func _on_analytics_error(msg) -> void:
	print ("ANALYTICS ERROR: ", msg)

func _on_game_ended():
	print ("Game ended.")
	_start_shutdown()

func _on_game_error(msg):
	print("GAME ERROR: ", msg)
	_start_shutdown()

func _start_shutdown(is_error: bool = false, msg: String = ""):
	var props := {
		is_error = is_error
	}
	if is_error and msg != "":
		props['error_message'] = msg

	# Ensure that closing analytics get sent.
	W4GD.analytics.stop_session(props)
	await W4GD.analytics.flush()

	# Start our shutdown timer to actually exit.
	$ShutdownTimer.start()

func _on_shutdown_timer_timeout():
	W4GD.game_server.set_server_state(W4GD.game_server.ServerState.SHUTDOWN)
